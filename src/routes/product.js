import { Router } from "express";
import {
  create,
  remove,
  get,
  getById,
  update,
} from "../controllers/Product.js";
import { checkPermission } from "../middlewares/checkPermission.js";
const routerProduct = Router();

routerProduct.post("/", create);
routerProduct.get("/", get);
routerProduct.get("/:slug", getById);
routerProduct.delete("/:id", remove);
routerProduct.put("/:id", update);

export default routerProduct;
