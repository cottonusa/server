import Joi from "joi";
export const productValidator = Joi.object({
  name: Joi.string().required().min(6).max(255),
  description: Joi.string().required(),
  price: Joi.number().required().min(0),
  discount: Joi.number().min(0),
  material: Joi.string().required(),
  variants: Joi.array()
    .items(
      Joi.object({
        color: Joi.string().required(),
        thumbnail: Joi.array()
          .items(
            Joi.object({
              imageUrl: Joi.string().required(),
            })
          )
          .required(),
        option: Joi.array()
          .items(
            Joi.object({
              size: Joi.string().required(),
              quantity: Joi.number().min(0).default(0),
            })
          )
          .required(),
      })
    )
    .required(),
  sku: Joi.string().required(),
  category: Joi.string().required(), // ObjectId
  collections: Joi.string().required(), // ObjectId
});
